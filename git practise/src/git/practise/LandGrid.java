/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package landgrid;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Anitha Kolluri
 */
public class LandGrid {

// change 3
    
    
    
    static int[][] gridArray;
    static double average;
    static int rows, cols;

    public static void main(String[] args) throws FileNotFoundException {
      
        
         Scanner stdin = new Scanner(new File("gridData.txt"));
        int it=1;
        while (stdin.hasNext()) {
            System.out.println("\nGRID LAYOUT : "+ it);
            rows = stdin.nextInt();
            cols = stdin.nextInt();

            gridArray = new int[rows + 2][cols + 2];

            //Adding an extra layer(first row,last row, first column and last column with dummy data around the 2DArray 
            for (int j = 0; j <= cols + 1; j++) {
                gridArray[0][j] = -1;
            }
            for (int j = 0; j <= cols + 1; j++) {
                gridArray[rows + 1][j] = -1;
            }
            for (int i = 0; i <= rows + 1; i++) {
                gridArray[i][0] = -1;
            }
            for (int i = 0; i <= rows + 1; i++) {
                gridArray[i][cols + 1] = -1;
            }

            System.out.println("The contents of the grid :");
            for (int i = 1; i <= rows; i++) {

                for (int j = 1; j <= cols; j++) {
                    gridArray[i][j] = stdin.nextInt();
                }
            }

            for (int i = 1; i <= rows; i++) {
                for (int j = 1; j <= cols; j++) {
                    System.out.printf("%3d " ,gridArray[i][j]);
                }
                System.out.println();
            }

            averageGridElevation();
            averageRowElevation();
            maxElevation();
            System.out.print("\nCo-ordinates of the peaks of elevation:");
            coordinatesOfPeaks();
            System.out.println();
            it++;
        }
    }
//method for row elevation
    //changes made
    public static void averageRowElevation() {
        int sum = 0;
        double avg;
        System.out.println();
        for (int i = 1; i <= rows; i++) {
            sum = 0;
            for (int j = 1; j <= cols; j++) {
                sum += gridArray[i][j];

            }
            avg = (double) sum / cols;
            System.out.println(String.format("Average elevation of row " + i + " is %.3f ", avg));
        }
    }

    public static void averageGridElevation() {
        int sum = 0, cellCount = rows * cols;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                sum += gridArray[i][j];
            }
        }
        average = (double) sum / cellCount;
        System.out.println(String.format("\nAverage elevation of the entire grid is %.2f ", average));

    }

    public static void maxElevation() {
        int max = 0;
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if (gridArray[i][j] > max) {
                    max = gridArray[i][j];
                }
            }
        }
        System.out.println("\nThe maximum elevation on the landmass is " + max);

    }

    public static void coordinatesOfPeaks() {
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                if ((i > 0 && i <= rows) && (j > 0 && j <= cols)) {
                    if (gridArray[i][j] > gridArray[i - 1][j - 1] && gridArray[i][j] > gridArray[i - 1][j + 1] && gridArray[i][j] > gridArray[i - 1][j]
                            && gridArray[i][j] > gridArray[i][j + 1] && gridArray[i][j] > gridArray[i][j - 1] && gridArray[i][j] > gridArray[i + 1][j - 1]
                            && gridArray[i][j] > gridArray[i + 1][j] && gridArray[i][j] > gridArray[i + 1][j + 1]) {
                        System.out.print("[" + (i - 1) + " ," + (j - 1) + "]");
                    }
                }

            }
        }

    }
    
}
